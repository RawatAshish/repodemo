var http = require('http');
var fs = require('fs');

http.createServer(function(req,res) {
	var url = req.url;
	switch(url){
		case'/':
			getStaticFileContent(res,'public/home.html','text/html');
			break;
		case'/about':
			getStaticFileContent(res,'public/about.html','text/html');
			break;
		case'/contact':
			getStaticFileContent(res,'public/contact.html','text/html');
			break;
		default:
			res.writeHead(404,{'content-Type':'text/plain'});
			res.end('404 - Page not found.')
	}
}).listen(3301);
console.log('Server running at http://localhost:3301');

function getStaticFileContent(res,filepath,contentType){
	fs.readFile(filepath, function(error, data){
		if(error){
			res.writeHead(500,{'content-Type':'text/plain'});
			res.end('500- Internal Server Error.');
		}
		if(data){
			res.writeHead(200,{'content-Type':'text/html'});
			res.end(data);
		}
	});
}